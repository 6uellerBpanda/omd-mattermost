#!/usr/bin/ruby
#
#
# Notification plugin for OMD and Mattermost
# ==
# Author: Marco Peterseil
# Created: 05-2017
# License: GPLv3 - http://www.gnu.org/licenses
# URL: https://gitlab.com/6uellerBpanda/omd-mattermost
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require 'optparse'
require 'net/https'
require 'json'

version = 'v0.1'

# optparser
banner = <<HEREDOC
omd-mattermost.rb #{version} [https://gitlab.com/6uellerBpanda/omd-mattermost]\n
Usage: #{File.basename(__FILE__)} [options]
HEREDOC

options = {}
OptionParser.new do |opts| # rubocop:disable Metrics/BlockLength
  opts.banner = banner.to_s
  opts.separator ''
  opts.separator 'Options:'
  opts.on('-m', '--mattermost MATTERMOST', 'Mattermost address') do |m|
    options[:mattermost] = m
  end
  opts.on('-t', '--token TOKEN', 'Access token') do |t|
    options[:token] = t
  end
  opts.on('-k', '--insecure', 'No ssl verification') do |k|
    options[:insecure] = k
  end
  opts.on('-c', '--notifytype NOTIFYTYPE', 'Notification type') do |c|
    options[:notifytype] = c
  end
  opts.on('-n', '--hostname HOSTNAME', 'Hostname') do |n|
    options[:hostname] = n
  end
  opts.on('-s', '--hoststate HOSTSTATE', 'Hoststate') do |s|
    options[:hoststate] = s
  end
  opts.on('-p', '--hostoutput HOSTOUTPUT', 'hostoutput') do |p|
    options[:hostoutput] = p
  end
  opts.on('-d', '--srvcdesc SRVCDESC', 'Servicedescription') do |d|
    options[:srvcdesc] = d
  end
  opts.on('-u', '--srvcstate SRVCSTATE', 'Servicestate') do |u|
    options[:srvcstate] = u
  end
  opts.on('-o', '--srvcoutput SRVCOUTPUT', 'Serviceoutput') do |o|
    options[:srvcoutput] = o
  end
  opts.on('-r', '--comment COMMENT', 'Comment') do |r|
    options[:comment] = r
  end
  opts.on('-a', '--author AUTHOR', 'Author') do |a|
    options[:author] = a
  end
  opts.on('-v', '--version', 'Print version information') do
    puts "omd-mattermost.rb #{version}"
    exit
  end
  opts.on('-h', '--help', 'Show this help message') do
    puts opts
    exit
  end
  ARGV.push('-h') if ARGV.empty?
end.parse!

# check args for api calls
unless ARGV.empty?
  raise OptionParser::MissingArgument if options[:mattermost].nil? || options[:token].nil?
end

# omd-mattermost
class OmdMattermost
  def initialize(options)
    @options = options
    post_data
  end

  # create url
  def create_url(payload, header)
    uri = URI("#{@options[:mattermost]}/hooks/#{@options[:token]}")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE if @options[:insecure]
    request = Net::HTTP::Post.new(uri.request_uri, header)
    request.body = payload.to_json
    @response = http.request(request)
  rescue => msg
    puts msg
  end

  # check some common http response
  def check_http_response
    case @response
    when Net::HTTPUnauthorized
      puts(@response.msg.to_s)
    when Net::HTTPNotFound
      puts(@response.msg).to_s
    when Net::HTTPBadRequest
      puts(@response.msg).to_s
    end
  end

  # init the http call
  def init_http(payload, header)
    create_url(payload, header)
    check_http_response
  end

  # set some emojis
  def set_emojis
    case @options[:notifytype]
    when 'PROBLEM' then @emoji = ':red_circle:'
    when 'RECOVERY' then @emoji = ':white_check_mark:'
    when 'DOWNTIMESTART' then @emoji = ':clock10:'
    when 'DOWNTIMEEND' then @emoji = ':sunny:'
    when 'ACKNOWLEDGEMENT' then @emoji = ':construction:'
    end
  end

  # create message
  def create_message
    @default_msg = "#### #{@emoji} #{@options[:notifytype]} - _#{@options[:hostname]}_\n"
    @host_msg = "##### Description\n#{@options[:hostoutput]}"
    @srvc_msg = "##### Check\n#{@options[:srvcdesc]} is #{@options[:srvcstate]}\n##### Description\n#{@options[:srvcoutput]}"
    return unless @options[:notifytype] == 'ACKNOWLEDGEMENT'
    @ack_msg = "\n##### Comment\nAuthor: #{@options[:author]}\n_#{@options[:comment]}_"
  end

  # put everything together
  def template
    set_emojis
    create_message
    if @options[:srvcstate].nil?
      @default_msg.to_s + @host_msg.to_s + @ack_msg.to_s
    else
      @default_msg.to_s + @srvc_msg.to_s + @ack_msg.to_s
    end
  end

  # post the message
  def post_data
    header = { 'Content-Type' => 'application/json' }
    payload = {
      'icon_url' => 'https://image.ibb.co/d7GAsa/omd_logo.png',
      'text' => template
    }
    init_http(payload, header)
  end
end # class end

OmdMattermost.new(options)
