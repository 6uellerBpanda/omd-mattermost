# OMD/Naemon Mattermost Plugin

[![build status](https://gitlab.com/6uellerBpanda/omd-mattermost/badges/master/build.svg)](https://gitlab.com/6uellerBpanda/omd-mattermost/commits/master)

A Plugin for [OMD](https://labs.consol.de/omd)/[Naemon](http://www.naemon.org) to send notifications to [Mattermost](https://about.mattermost.com/)

Should also work for other Naemon compatible software like Nagios/Icinga1 but untested from my side.

Tested with: OMD 2.40/Naemon 1.0.6, Ruby 2.1.5, Mattermost 3.7.3

# Usage
```shell_session
./omd-mattermost.rb
omd-mattermost.rb v0.1 [https://gitlab.com/6uellerBpanda/omd-mattermost]

Usage: omd-mattermost.rb [options]

Options:
    -m, --mattermost MATTERMOST      Mattermost address
    -t, --token TOKEN                Access token
    -k, --insecure                   No ssl verification
    -c, --notifytype NOTIFYTYPE      Notification type
    -n, --hostname HOSTNAME          Hostname
    -s, --hoststate HOSTSTATE        Hoststate
    -p, --hostoutput HOSTOUTPUT      hostoutput
    -d, --srvcdesc SRVCDESC          Servicedescription
    -u, --srvcstate SRVCSTATE        Servicestate
    -o, --srvcoutput SRVCOUTPUT      Serviceoutput
    -r, --comment COMMENT            Comment
    -a, --author AUTHOR              Author
    -v, --version                    Print version information
    -h, --help                       Show this help message

```

## Options
-m: Mattermost server URL, only https supported, https://mattermost.example.com

-k: if you've a self signed cert

-t: access token/key, the long string at the end of incoming webhook URL

# Mattermost configuration
You need to enable [Incoming Webhooks](https://docs.mattermost.com/developer/webhooks-incoming.html#enabling-incoming-webhooks) for Mattermost server.
Only token based authentication and HTTPS is supported at the moment.

# Naemon configuration
## Notification commands
```conf
# etc/naemon/conf.d/notification_commands_mattermost.cfg

define command {
  command_name notify-service-by-mattermost
  command_line $USER1$/omd-mattermost.rb -m [MATTERMOST_URL] -t [TOKEN] \
    --notifytype "$NOTIFICATIONTYPE$" \
    --hostname "$HOSTNAME$" \
    --srvcdesc "$SERVICEDESC$" \
    --srvcstate "$SERVICESTATE$" \
    --srvcoutput "$SERVICEOUTPUT$" \
    --comment "$NOTIFICATIONCOMMENT$" \
    --author "$NOTIFICATIONAUTHOR$"
}

define command {
  command_name notify-host-by-mattermost
  command_line $USER1$/omd-mattermost.rb -m [MATTERMOST_URL] -t [TOKEN] \
    --notifytype "$NOTIFICATIONTYPE$" \
    --hostname "$HOSTNAME$" \
    --hoststate "$HOSTSTATE$" \
    --hostoutput "$HOSTOUTPUT$" \
    --comment "$NOTIFICATIONCOMMENT$" \
    --author "$NOTIFICATIONAUTHOR$"
}
```
## Contacts
```conf
define contact {
  contact_name                    mattermost
  alias                           mattermost
  service_notification_period     24x7
  host_notification_period        24x7
  service_notification_options    w,u,c,r
  host_notification_options       d,r
  host_notification_commands      notify-host-by-mattermost
  service_notification_commands   notify-service-by-mattermost
}

define contactgroup {
  contactgroup_name   omd-admins
  alias               omd-admins
  members             mattermost
}
```

# Example
## Problem
```shell_session
./omd-mattermost.rb -m https://mattermost.example.com -t 1d37qbjyutgrmn6c6bxoyiqxkc \
--notifytype PROBLEM --hostname dc01.test.at \
--srvcdesc check_ntp --srvcstate CRITICAL \
--srvcoutput 'NTP CRITICAL: No response from NTP server'
```
![alt text](img/screenshot.png)
## Acknowledgements
Notifcations with type of _Acknowledgement_ will have author and comment text displayed
```shell_session
./omd-mattermost.rb -m https://mattermost.example.com -t 1d37qbjyutgrmn6c6bxoyiqxkc \
--notifytype PROBLEM --hostname dc01.test.at \
--srvcdesc check_ntp --srvcstate CRITICAL \
--srvcoutput 'NTP CRITICAL: No response from NTP server' \
--author omd-admin --comment 'I will take a look'
```
![alt text](img/screenshot_ack.png)
